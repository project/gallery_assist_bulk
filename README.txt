
Gallery Assist
--------------

This module helps administrators to take mass-changes to the GA-settings.


Incompatibilities
-----------------
* I would be very grateful if you give us feedback in case you get errors or find issues.
I thank you in advance.


Maintainer
----------
The Gallery Assist Bulk Operations module was originally developped by:
Juan Carlos Morejon Caraballo

Current maintainer:
Juan Carlos Morejon Caraballo
